#include <stdio.h>
#include <unistd.h>

#define TAM 10

/*
    6.1.1 - Primeira Tentativa - Alocador Ingenuo


    Log Problemas: (Aparentemente tudo funciona como deveria, exceto que...)
        Segundo o valgrind:
            o sistema operacional aumenta a heap 1 unica vez, em 1024 Bytes...
                De fato, a printf eh quem aloca essa memoria
            Segundo o manual do valgrind, ele nao vai detectar o meu alocador automaticamente...
                Por enquanto, vamos ter q viver com isso...

        sbrk(0) nao muda durante a execucao do programa...
            Na verdade, ele aumenta <- printf aparentemente aloca mem na heap, na 1 chamada
                Menos quando eu chamo o valgrind...
*/

extern void iniciaAlocador();

extern void finalizaAlocador();

extern void *alocaMem(int num_bytes);

extern void liberaMem(void *bloco); 

int main() {
    int i, j, size;
    char *ptr[TAM];

    printf("Inicio do program: Heap - %p\n", sbrk(0)); //printf USA HEAP

    puts("Inicializando alocador...");
    printf("Topo da heap: %p\n", sbrk(0));

    iniciaAlocador();

    puts("Iniciando alocacoes...");

    for (i = 0; i < TAM; i++) {
        size = sizeof(char) * i + 1;
        ptr[i] = alocaMem(size);

        for (j = 0; j < i; j++)
            ptr[i][j] = 'x';
        ptr[i][i] = '\0';
        
        printf("Alocacao %d | Size %2d | End Retorno %p\n", i, size, ptr[i]);
    }

    puts("liberando..."); //Nao faz nada

    for (i = 0; i < TAM; i++) {
        printf("End %p | str:(%s)\n", ptr[i], ptr[i]);
        liberaMem(ptr);
    }

    puts("Finalizando alocador...");
    finalizaAlocador();

    puts("Melhor q a stdlib");

    return 0;
}

