/*
    6.1.1 Primeira Tentativa - Abordagem Ingenua
        Registradores sao preservados seguindo a ABI64

*/

.section .data
    topoInicialHeap: .quad 0

.section .text

.globl iniciaAlocador

iniciaAlocador: # void iniciaAlocador() : executa syscall brk para obter o endereço do topo corrente da heap
    movq $12, %rax  # Codigo da chamada Brk
    movq $0,  %rdi  # Retorna o valor atual da brk em %rax
    syscall          

    movq %rax, topoInicialHeap # Salva o topo da Heap

    ret

.globl finalizaAlocador

finalizaAlocador: # void finalizaAlocador() : executa syscall brk para restaurar o valor original da heap
    movq $12, %rax              # Codigo da chamada
    movq topoInicialHeap, %rdi  # Restaura o topo da Heap
    syscall 

    ret

.globl alocaMem

alocaMem: # void *alocaMem(int num_bytes) : executa syscall brk para abrir um bloco de num_bytes
    pushq %rbp           # Informacoes Gerenciais - Registro de ativacao
    movq %rsp, %rbp

    subq $16, %rsp       # Aumenta Pilha
    movq %r12, -16(%rbp) # Reg Presevado
    movq %rbx, -8(%rbp)  #  -

    movq %rdi, %rbx      # rbx = num_bytes

    movq $12, %rax       # Codigo da chamada Brk
    movq $0, %rdi        # Retorna o valor atual da brk em %rax
    syscall              

    movq %rax, %r12      # r12 = "Inicio Bloco"
    addq %rax, %rbx      # rbx = "Novo Topo"

    movq $12,  %rax      # Codigo da chamada Brk
    movq %rbx, %rdi      # rdi = "NovoTopo"
    syscall

    movq %r12, %rax      # rax = "Inicio Bloco"

    movq -16(%rbp), %r12 # Restaurando registradores
    movq -8(%rbp),  %rbx #  -
    addq $16, %rsp       # Restaura a pilha

    popq %rbp            # Desmontando Registro de Atv
    ret

.globl liberaMem

liberaMem: # int liberaMem(void *bloco) : nao faz nada
    ret

