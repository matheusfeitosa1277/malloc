#include <stdio.h>
#include <unistd.h>

#define TAM 5

/*
    6.1.2 - Segunda Tentativa - Alocador de Bartlett
        Main: Adaptado do livro - Alg 6.3

    Log Problemas: Organizacao...
*/

extern void iniciaAlocador();

extern void finalizaAlocador();

extern void *alocaMem(int num_bytes);

extern void liberaMem(void *bloco); 

extern void imprimeMapa();

/*
    Variaveis internas do Asm - Nao alterar
*/

extern long int inicHeap, topoHeap;

enum fit {FIRST, BEST, NEXT};

/*
    Variaveis globais C
*/

void *slot[TAM], *j;
int i;
long int aux, size;

void t1() {
    puts("\nTeste 1 - Alocando / desalocando sequencialmente");
    puts("End Ret    | Desloc   | offSet   | tam solicitado");

    i = 0;
    do {
        size = 2048 * i;
        slot[i] = alocaMem(size); 
        
        aux = slot[i] - j;
        printf("%10p | %8p | %8d | %d\n", slot[i], aux, aux, size);

        j = slot[i];
        ++i;
    } while (i < TAM);

    //imprimeMapa();

    for (i = 0; i < TAM; i++)
        liberaMem(slot[i]);

    printf("\nHeap: %p %p\n", inicHeap, topoHeap);
}

void t2() {
    puts("\nTeste 2 - Alocando / desalocando do \"final\" para o inicio ");
    puts("End Ret    | tam Soli");

    i = 0;
    do {
        size = 8192 - 2048 * i;
        slot[i] = alocaMem(size); 
        
        printf("%10p | %d\n", slot[i], size); ++i; } while (i < TAM);

    //imprimeMapa();

    for (i = 0; i < TAM; i++)
        liberaMem(slot[i]);
}

void t3() {
    puts("\nTeste 3 - Alocando / desalocando com espacamento na heap");

    i = 0;
    do {
        size = 2048 + 4096 * i;
        slot[i] = alocaMem(size); 
        
        printf("%10p | %d\n", slot[i], size);

        ++i;
    } while (i < 2);

    //imprimeMapa();

    for (i = 0; i < 2; i++)
        liberaMem(slot[i]);

}

void t4() {
    puts("\nTeste 4 - Alocando / desalocando com espacamento na heap");

    i = 0;
    do {
        size = 4096 * i;
        slot[i] = alocaMem(size); 
        
        printf("%10p | %d\n", slot[i], size);

        ++i;
    } while (i < 3);

    //imprimeMapa();

    for (i = 0; i < 3; i++)
        liberaMem(slot[i]);

}

int main() {
    printf("Inic Heap:     %p\n", sbrk(0));

    j = sbrk(0);

    printf("Inic Alocador: %p\n", j);
    iniciaAlocador();

    t1();

    t2();

    t3();

    t4();

    finalizaAlocador();


    return 0;
}

