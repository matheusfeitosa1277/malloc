/*
    6.1.2 Segunda Tentativa - Alocador de Bartlett 
        Registradores sao preservados seguindo a ABI64

        Obs: iniciaAlocador e finalizaAlocador sao semelhantes a abordagem ingenua...
*/

.section .data
    inicHeap: .quad 0
    topoHeap: .quad 0

    lastSrch: .quad 0

.section .text

/* Visivel C */
.globl inicHeap

.globl topoHeap
/* -------- */

.globl iniciaAlocador

iniciaAlocador: # void iniciaAlocador() : executa syscall brk para obter o endereço do topo corrente da heap
    movq $12, %rax  # Codigo da chamada Brk
    movq $0,  %rdi  # Retorna o valor atual da brk em %rax
    syscall          

    movq %rax, topoHeap # Salva o topo da Heap
    movq %rax, inicHeap 

    //
    movq inicHeap, %rdi # Facilita a funcao nextFit...
    movq %rdi, lastSrch 
    //

    ret

.globl finalizaAlocador

finalizaAlocador: # void finalizaAlocador() : executa syscall brk para restaurar o valor original da heap
    movq $12, %rax       # Codigo da chamada
    movq inicHeap, %rdi  # Restaura o topo da Heap
    syscall 

    ret

.globl liberaMem

liberaMem: # int liberaMem(void *bloco)
    movq $0, -16(%rdi) # "Bloco Desocupado"

    ret

.globl alocaMem

# 1. Procura um bloco livre com tamanho maior ou igual à num_bytes.
# 2. Se encontrar, indica que o bloco está ocupado e retorna o endereço inicial do mesmo
# 3. Se não encontrar, abre espaço para um novo bloco usando a syscall brk,
#      indica que o bloco está ocupado e retorna o enderco inicial do mesmo

alocaMem: # void *alocaMem(int num_bytes)
    pushq %rbp           # Informacoes Gerenciais - Registro de ativacao
    movq %rsp, %rbp

    movq inicHeap, %rax  # rax = "Inicio Lista Encadeada"

    busca:
       #jmp firstFit
       #jmp bestFit
        jmp nextFit

    brk: 
        jmp sbrk

    desmanche:
        popq %rbp        # Desmontando Registro de Atv
        ret

/*
    Vamos quebrar as convencoes com call e usar um simples jump...
*/

# Syscall brk 6.2.c)
#  minimize o número de chamadas ao serviço brk alocando espaços múltiplos de 4096 bytes por vez. Se for
#   solicitado um espaço maior, digamos 5000 bytes, então será alocado um espaço de 4096 ∗ 2 = 8192 bytes
#   para acomodá-lo

    /* %rax = topoHeap - %rdi = num_bytes */

sbrk: 
    subq $16, %rsp          # Preenche o registro de ativacao

    addq $16, %rax          # rax = &bloco (Falta alocar)
    movq %rax, -8(%rbp)     # &bloco

    movq %rdi, %rax # %rax = num_bytes
    movq $4096, %rdi  
    div %rdi        # %rax = num_bytes div 4096 
    
    cmpq $0, %rdx    # Divisao exata?
    je resto        # Sim
    addq $1, %rax   # Nao, soma 1
    resto:
        mul %rdi    # %rax *= %rdi 

    movq %rax, -16(%rbp)    # tamAloc

    movq %rax, %rdi         # rdi = SIZE

    movq $12, %rax          # Codigo da chamada Brk
    addq $16, %rdi          # espaco informacoes gerencias
    addq topoHeap, %rdi     # rdi = topoHeap + num_bytes + gerenciais

    movq %rdi, topoHeap     # Atualiza topoHeap
    syscall

    movq -8(%rbp), %rax     # rax = &bloco
    movq $1, -16(%rax)      # Ocupado

    movq -16(%rbp), %r10    #  r10  = tamAloc
    movq %r10, -8(%rax)     # *SIZE = r10

    addq $16, %rsp          # Restaura a pilha

    jmp desmanche


# Funcoes de Busca
    /* %rax = topoHeap - %rdi = num_bytes */

# Completo
firstFit: # percorre a lista do início e escolhe o primeiro nó com tamanho maior ou igual ao solicitado
    cmpq %rax, topoHeap # Compare rax com topoHeap
    je brk              # Chegou no fim e nao achou, aloca memoria

    addq $16, %rax      # rax = &Bloco

    cmpq $1, -16(%rax)  # F == 1 ? - Cuidado com a eflag ZF
    je firstInvalido

    firstLivre: 
        cmpq %rdi, -8(%rax) # SIZE >= num_bytes ?
        jl firstInvalido    # Falta Memoria
        
        movq $1, -16(%rax)  # F = 1

        jmp desmanche                   

    firstInvalido:
        addq -8(%rax), %rax # rax += "Salto ate a proxima Flag"
        jmp firstFit 

# Completo
    /* %r9 = tamMelhorNo - %r10 = &bloco */

bestFit: # percorre toda a lista e seleciona o nó com menor bloco, que é maior ou igual ao solicitado
    movq $4294967295, %r9 # Controle - Melhor No - Max long int
    movq $0, %r10         # &Bloco

    bestLoop: 
        cmpq %rax, topoHeap 
        je escolha          # Olhou todos os blocos

        addq $16, %rax      # rax = &Bloco

        cmpq $1, -16(%rax)  # F == 1 ? - Cuidado com a eflag ZF
        je bestInvalido

        bestLivre:
            cmpq %rdi, -8(%rax) # SIZE >= num_bytes ?
            jl bestInvalido     # Falta Memoria

            cmp -8(%rax), %r9   # Bloco eh melhor? (menos memoria)
            jle bestInvalido    # Nao

            movq -8(%rax), %r9  # Sim, atualiza melhor
            movq %rax, %r10

        bestInvalido:
            addq -8(%rax), %rax # rax += "Salto ate a proxima Flag"
            jmp bestLoop

        escolha:
            cmp $0, %r10
            je brk              # Nao achou um bloco

            movq $1, -16(%r10)  # Melhor bloco ocupado
            movq %r10, %rax

            jmp desmanche

# Problemas: Implementacao ingenua

nextFit: # e first fit em uma lista circular. A busca começa onde a última parou
    movq $0, %rsi       # Flag controle
    movq lastSrch, %rax # Comeca a busca de onde parou

    nextBusca:
        cmpq topoHeap, %rax # Compare rax com topoHeap
        je nextInic         # Overflow e nao achou, volta para o inicio

        addq $16, %rax

        cmpq $1, -16(%rax)  # F == 1 ? - Cuidado com a eflag ZF
        je nextInvalido

        nextLivre: 
            cmpq %rdi, -8(%rax) # SIZE >= num_bytes ?
            jl nextInvalido     # Falta Memoria
            
            movq $1, -16(%rax)  # F = 1
            
            subq $16, %rax

            movq %rax, lastSrch

            addq $16, %rax

            jmp desmanche                   

        nextInvalido:
            addq -8(%rax), %rax # rax += "Salto ate a proxima Flag"

            jmp nextBusca

        nextInic:
            cmpq $1, %rsi
            je nextAlloc

            movq inicHeap, %rax # Recomeca a busca

            movq $1, %rsi

            jmp nextBusca

        nextAlloc:
            movq %rax, lastSrch
            jmp brk


/* ------------------------------------------------------------------------------------ */

/*
    6.4
     implemente um procedimento que imprime um mapa da memória da região da heap em todos os algoritmos
     propostos aqui. Cada byte da parte gerencial do nó deve ser impresso com o caractere "#".
     O caractere usado para a impressão dos bytes do bloco de cada nó depende se o bloco estiver livre ou ocupado.
     Se estiver livre, imprime o caractere "-". Se estiver ocupado, imprime o caractere "+".
*/

# Talvez seja valido criar um arquivo so pra esse procedimento...

/*
    Problemas: Implementacao ingenua
        Utilizando putchar ao inves de syscall
*/

/* %r12 = &bloco - %r13 = SIZE - %r14 = char */

.globl imprimeMapa

imprimeMapa: 
    pushq %rbp
    movq %rsp, %rbp

    pushq %r12 
    pushq %r13
    pushq %r14

    movq inicHeap, %r12

    writeMap:
        cmpq topoHeap, %r12 # Escreveu todos os blocos?
        je endMap           # Sim

        mov $0x23, %rdi 
        call putchar
        mov $0x23, %rdi 
        call putchar

        addq $16, %r12       # %r12 = &bloco

        writeBlock:
            movq -8(%r12), %r13  # %r13 = SIZE

            movq $0x2b, %r14     # Assume que o bloco esta ocupado <- +

            cmpq $1, -16(%r12) # Se estiver livre: 
            je busy

            movq $0x2d, %r14     # Muda o char <- - 


            busy:
                cmpq $0, %r13    # Escreveu todos os chars?
                je endBlock

                movq %r14, %rdi

                call putchar

                subq $1, %r13
                jmp busy

        endBlock:
            addq -8(%r12), %r12 # %r12 = &proxBloco
            jmp writeMap

    endMap:
        movq $0xa, %rdi
        call putchar

        movq -8(%rbp), %r12
        movq -16(%rbp), %r13
        movq -24(%rbp), %r14

        movq %rbp, %rsp
        popq %rbp
        ret

/* ------------------------------------------------------------------------------------ */

/*
    Outras Versoes...

    Aumenta Heap em num_bytes - 6.1 

    brk: # Aloca - r10 e um reg tmp - %rax = topoHeap
        subq $16, %rsp          # Preenche o registro de ativacao
        addq $16, %rax          # rax = &Bloco (Falta alocar)

        movq %rdi, -16(%rbp)    # num_bytes
        movq %rax, -8(%rbp)     # topoHeap

        movq $12, %rax          # Codigo da chamada Brk
        addq $16, %rdi          # espaco informacoes gerencias
        addq topoHeap, %rdi     # rdi = topoHeap + num_bytes + gerenciais

        movq %rdi, topoHeap     # Atualiza topoHeap
        syscall

        movq -8(%rbp), %rax     # rax = &Bloco
        movq $1, -16(%rax)      # Ocupado

        movq -16(%rbp), %r10    #  r10  = num_bytes
        movq %r10, -8(%rax)     # *SIZE = num_bytes

        addq $16, %rsp          # Restaura a pilha
*/

